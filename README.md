# **Fork me on chaos.expert** CSS ribbon

This is a __*Fork me on chaos.expert*__ ribbon, like the popular _Fork me on G** H**_ ribbons. It's written in pure css and comes without any dependencies.

## Screenshot

![screenshot](img/screenshot.png "Screenshot of Fork me on chaos.expert in action")
This is a screenshot of the example.html which is included in this repository.

## Installation

### Manual

Just clone or download this repository and copy both, the `chaos-expert-fork-ribbon.css` and the `chaos-expert-fork-ribbon.ie.css`, into the folder holding the `.css` files of your project.

### Using with Bower

You can install the CSS files using [Bower](https://github.com/bower/bower).

```bower install chaos-expert-fork-ribbon```

## Integration

Put the .css files provided in this repro into the folder where you store all the other css stuff and include the
 following lines into your HTML-*Code*

Copy the following code into the `<head>` of your pag and replace the path with the actual location of the `.css` files.

```html
<link rel="stylesheet" href="<path>/<to>/<your>/<css>/chaos-expert-fork-ribbon.css" />
<!--[if lt IE 9]>
    <link rel="stylesheet" href="<path>/<to>/<your>/<css>/chaos-expert-fork-ribbon.ie.css" />
<![endif]-->
```

And copy the following line of code into the `<body>` of your page and you are done. But don't forget to put the url of your repo into the `href`. Otherwise it would link to, ermmm… nothing.

```html
<a class="chaos-expert-fork-ribbon" href="http://url.to-your.repo.example" title="Fork me on chaos.expert">Fork me on chaos.expert</a>
```

### colors
Currently the following colors are implemented:

- default (pink)
- red
- green
- blue
- yellow
- grey
- white (for darker background)

To use a certain color simply put the name of the color (see list above) behind the class name `chaos-expert-fork-ribbon`. 

#### Example

For a green ribbon you would have to use:
``` html
<a class="chaos-expert-fork-ribbon green" href="https://chaos.expert/chaos-expert" title="Fork me on chaos.expert"></a>
```

## Credits

This work is based on [Simon Whitakers](https://twitter.com/s1mn) work which you'll find on [Github](https://github.com/simonwhitaker/github-fork-ribbon-css)

Feel free to fork, tweak and send me a merge request. The whole stuff is licensed under the MIT license.
