# Changelog

## v0.1.1 (2016-10-01)
- orange color scheme added

## v0.1.0 (2016-08-19)
- color schemes added
  - pink (default)
  - red
  - green
  - blue
  - yellow
  - white
  - grey

## v0.0.1 (2016-08-19)
- initial version
